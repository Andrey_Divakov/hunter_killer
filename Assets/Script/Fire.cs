﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Fire : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    float tim = 0.4f;
    public bool fireP,luch;

    private void Start()
    {
        luch = true;
        fireP = false;
    }
    private void Update()
    {
        if (fireP)
        {
            if (tim < 0)
            {
                Shoot();
                tim = 0.4f;
            }
            tim -= Time.deltaTime;
        }
    }
    public void Shoot()
    {
     GameObject pul= Instantiate(bulletPrefab,new Vector3(0f,0f,0f),firePoint.rotation);
        pul.transform.SetParent(firePoint);
        pul.transform.localPosition = Vector3.zero;
        pul.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        Destroy(pul, 2f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            fireP = true;
            luch = false;
            transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color32(250, 8, 8, 102);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            fireP = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            luch = true;
            fireP = false;
            gameObject.GetComponent<PlayerMove>().target.gameObject.name = "1";
        }
    }
}
