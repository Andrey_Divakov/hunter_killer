﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pula : MonoBehaviour
{
    private void FixedUpdate()
    {
        transform.localPosition += new Vector3(0f,0.2f,0f);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            GetComponent<MeshRenderer>().enabled = false;
            Destroy(gameObject, 3f);
        }
            
    }
}
