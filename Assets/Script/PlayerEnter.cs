﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEnter : MonoBehaviour
{
    public GameObject slide,money,canva,nummber;
    byte ko;
    private void Start()
    {
        ko = 0;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.isTrigger != true)
        {
            if (collision.gameObject.tag == "Terror")
            {
                GameObject k = Instantiate(money,collision.gameObject.transform.localPosition,money.transform.rotation);
                Destroy(collision.gameObject.GetComponent<PlayerMove>().target.gameObject);
                Destroy(collision.gameObject);
                Destroy(k, 1f);
                ko++;
                slide.transform.GetChild(2).gameObject.GetComponent<Text>().text = "4/" + ko.ToString();
                if (ko == 4)
                {
                    if (slide.transform.GetChild(0).gameObject.GetComponent<Slider>().value != 100)
                    {
                        PlayerPrefs.SetFloat("record", 2f);
                        PlayerPrefs.Save();
                        slide.transform.GetChild(3).gameObject.SetActive(true);
                        canva.transform.GetChild(0).gameObject.SetActive(true);
                        canva.transform.GetChild(1).gameObject.SetActive(true);
                    }
                }
            }
            else if (collision.gameObject.tag == "Fire")
            {
                slide.transform.GetChild(0).GetChild(1).GetChild(0).gameObject.SetActive(true);
                slide.transform.GetChild(0).gameObject.GetComponent<Slider>().value += 10f;
                collision.gameObject.GetComponent<MeshRenderer>().enabled = false;
                Destroy(collision.gameObject,3f);
                GameObject kik = Instantiate(nummber, transform.position, Quaternion.identity);
                kik.transform.SetParent(transform);
                kik.transform.localRotation = Quaternion.Euler(0, 0, 0);
                Destroy(kik, 1f);
                if (slide.transform.GetChild(0).gameObject.GetComponent<Slider>().value == 100)
                {
                    slide.transform.GetChild(4).gameObject.SetActive(true);
                    canva.transform.GetChild(1).gameObject.SetActive(true);
                }
            }
        }
    }
}
