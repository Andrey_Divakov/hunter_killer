﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class TerrorMove : MonoBehaviour
{
    private NavMeshAgent agent;
    public GameObject player;
    float g,tim;

    void Start()
    {
        tim = 1f;
        if (name == "Terro1")
            g = -150f;
        else if (name == "Terro2")
            g = 50f;
        else if (name == "Terro3")
            g = -50f;
        else g = 150f;
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(new Vector3(Random.Range(-3f,3f),transform.position.y,
            Random.Range(-3f, 6f)));
    }
    void Update()
    {
        if (name == "1")
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Ground" | hit.collider.tag == "Terror")
                {
                    agent.SetDestination(hit.point);
                }
            }
            name = "3";
        }

        if (agent.velocity.x == 0)
        {
            player.GetComponent<Animator>().enabled = false;
            if (player.GetComponent<Fire>().luch == true)
            {
                player.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 102);
            }

            if (player.GetComponent<Fire>().fireP == false)
            {
                g += 1f;
                transform.localRotation = Quaternion.Euler(0, g, 0);
                if (tim < 0)
                {
                    agent.SetDestination(new Vector3(Random.Range(-3f, 3f), transform.position.y,
                                Random.Range(-3f, 6f)));
                    tim = Random.Range(3f, 6f);
                }
                tim -= Time.deltaTime;
            }

        }
        else
            player.GetComponent<Animator>().enabled = true;
    }
}
