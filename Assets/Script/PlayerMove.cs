﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Transform target;
    private void LateUpdate()
    {
        transform.localPosition = new Vector3(target.localPosition.x,
            transform.localPosition.y, target.localPosition.z);
        transform.rotation = Quaternion.Euler(90f, target.rotation.eulerAngles.y, 
target.rotation.eulerAngles.z);
    }
}
