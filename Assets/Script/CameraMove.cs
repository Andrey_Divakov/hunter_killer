﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public Transform plaPos;

    void FixedUpdate()
    {
        if (transform.position.z < -2f)
            transform.position = new Vector3(transform.position.x,50f,-2f);
        if (transform.position.z > 4f)
            transform.position = new Vector3(transform.position.x, 50f, 4f);
        if (transform.position.x < -1f)
            transform.position = new Vector3(-1f, transform.position.y,transform.position.z);
        if (transform.position.x > 1f)
            transform.position = new Vector3(1f, transform.position.y,transform.position.z);
        transform.position = Vector3.Slerp(transform.position,
            new Vector3(plaPos.position.x,transform.position.y,plaPos.transform.position.z),2f*Time.fixedDeltaTime);
    }
}
