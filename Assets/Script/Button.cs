﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Button : MonoBehaviour
{
    private void Start()
    {
        if (!PlayerPrefs.HasKey("record"))
        {
            PlayerPrefs.SetFloat("record", 1f);
            PlayerPrefs.Save();
        }
        if (name != "Canvas1")
        {
            if (PlayerPrefs.GetFloat("record") == 2f)
            {
                transform.GetChild(2).GetChild(1).gameObject.GetComponent<Image>().color =
                    Color.green;
            }
            else transform.GetChild(2).GetChild(1).gameObject.GetComponent<Image>().color =
                    Color.red;
        }
    }
    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(true);
    }
    public void Button_Play()
    {
        string YOUR_APP_KEY = "b0e3d5f5";
        IronSource.Agent.init(YOUR_APP_KEY, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.OFFERWALL, IronSourceAdUnits.BANNER);
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.GetComponent<Text>().text = "Level";
        transform.GetChild(2).gameObject.SetActive(true);
    }
    public void Button_Level1()
    {
        SceneManager.LoadScene("Level1");
    }
    public void Button_Home()
    {
        SceneManager.LoadScene("Menu");
    }
    public void Button_Level2()
    {
        if (PlayerPrefs.GetFloat("record") == 2f)
        {
            SceneManager.LoadScene("level2");
        }
    }
}
