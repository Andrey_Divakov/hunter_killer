﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    private NavMeshAgent agent;
    public GameObject circl,player;
    LineRenderer linred;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        linred = GetComponent<LineRenderer>();

    }
    // Update is called once per frame
    void Update()
    {
        if(player.GetComponent<PlayerEnter>().slide.transform.GetChild(0).
            gameObject.GetComponent<Slider>().value != 100)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.tag != "Untagged")
                    {
                        agent.SetDestination(hit.point);
                        linred.positionCount = agent.path.corners.Length;
                        linred.SetPositions(agent.path.corners);
                        GameObject cir = Instantiate(circl, hit.point, Quaternion.identity);
                        Destroy(cir, 0.3f);
                    }
                }
            }
            linred.positionCount = agent.path.corners.Length;
            linred.SetPositions(agent.path.corners);
            if (agent.velocity.x == 0)
                player.GetComponent<Animator>().enabled = false;
            else
                player.GetComponent<Animator>().enabled = true;
        }
    }
}
